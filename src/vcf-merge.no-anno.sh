#!/usr/bin/env bash
#Felix Richter
#6/23/2014
#processing of bash files after running pipeline

#ssh interactive1
#make sure there is no other anno screen running, kill it if it's done
#screen -R -D anno
#bash vcf-merge.no-anno.sh 2>&1 | tee vcf-merge.no-anno.err_out.txt

SAMPLE_DIR='/sc/orga/projects/chdiTrios/PBG/PCGC'
DATA_FILE='/sc/orga/work/landyk01/Gelblab/mosaicism/data/sample_id.uzilov.1.10.txt'
#DATA_FILE='/hpc/users/richtf01/gelb/mosaicism/data/sample_id.uzilov.1.10.txt'
OUT_DIR='/sc/orga/work/landyk01/Gelblab/mosaicism/results'
#OUT_DIR='/hpc/users/richtf01/gelb/mosaicism/results'
OUT_FILE="$OUT_DIR/out_file.txt"
rm -rf $OUT_DIR
mkdir $OUT_DIR

module load ngs/2.7.2b
module load vcftools/0.1.12a
while read SAMPLE_NAME; do
    echo $SAMPLE_NAME
    #merge files
    rm -R $SAMPLE_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/
    mkdir -p $SAMPLE_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results/
    cd $SAMPLE_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results/
    cp $SAMPLE_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/$SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.01.vcf .
    cp $SAMPLE_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/$SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.02.vcf .
    bgzip $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.01.vcf
    tabix -p vcf $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.01.vcf.gz
    bgzip $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.02.vcf
    tabix -p vcf $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.02.vcf.gz
    vcf-merge $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.01.vcf.gz $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.02.vcf.gz \
    | bgzip -c > $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.0102.vcf.gz

    #decompress everything
    bgzip -d $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.01.vcf.gz
    bgzip -d $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.02.vcf.gz
    bgzip -d $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.0102.vcf.gz

    vcftools --vcf $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.0102.vcf --extract-FORMAT-info GT --out merge
    grep -E '0\/1\s0\s0\/1\s0' merge.GT.FORMAT > merge.singletons
    awk '/^#/ {print $0}' $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.0102.vcf > $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.0102.singletons.vcf
    awk 'NR==FNR{a[$1,$2]=$3;next} ($1,$2) in a{print $0, a[$1,$2]}' \
    merge.singletons $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.0102.vcf \
    >> $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.0102.singletons.vcf

    grep '#CHROM' $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.0102.singletons.vcf | tee $OUT_DIR/$SAMPLE_NAME.txt \
    | tee -a $OUT_FILE
    grep -v '#' $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.0102.singletons.vcf | \
    grep -v -E '\.\s\.' | tee -a $OUT_DIR/$SAMPLE_NAME.txt | tee -a $OUT_FILE

    chmod -R a+rwx ${DATA_DIR}/${SAMPLE_NAME}/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/

done < $DATA_FILE

grep -v '#' $OUT_FILE | wc -l
#to eliminate files that had trouble processing, use: find . | xargs wc -l
