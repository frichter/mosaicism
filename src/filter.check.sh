#!/usr/bin/env bash
#filter the anno file
#Note that the mutect FORMAT tags my awk is for is GT:AD:BQ:DP:FA:SS
#compare number of mutations in files post-filtering: should be the same!
#bash filter.check.sh | column -t
DATA_FILE='/hpc/users/richtf01/gelb/mosaicism/data/sample_id.uzilov.1.10.txt'

while read SAMPLE_NAME; do
    echo $SAMPLE_NAME
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/
    wc -l < $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.01.vcf
    grep -A 5 '#CHROM' $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.01.vcf
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/
    wc -l < $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.02.vcf
    grep -A 5 '#CHROM' $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.02.vcf
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/02324/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results/
    grep -A 5 '#CHROM' $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.0102.singletons.vcf
    
done < $DATA_FILE
