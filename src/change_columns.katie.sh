#!/usr/bin/env bash
#$1 is file

#COL_10=$(grep '#CHROM' $1 | cut -f 10)
#COL_11=$(grep '#CHROM' $1 | cut -f 11)

COL_10=$(grep 'chr' $1 | cut -f 10)
COL_11=$(grep 'chr' $1 | cut -f 11)

#if [[ $COL_10 =~ [0-9]{5}\-[0-9]{2} ]] ; then
if [[ $COL_10 =~ 
    grep -v ^'##' $1 | \
    awk -F '\t' 'BEGIN{OFS="\t"} { print $1,$2,$3,$4,$5,$6,$7,$8,$9,$11,$10}'
fi

if [[ $COL_11 =~ [0-9]{5}\-[0-9]{2} ]] ; then 
    grep -v ^'##' $1 | \
    awk -F '\t' 'BEGIN{OFS="\t"} { print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11}'
fi
