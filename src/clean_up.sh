#!usr/env/bin bash
#clean up job

while read SAMPLE_NAME; do
    echo $SAMPLE_NAME
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/
    #P1
    rm -rf NGS.2_7_2b.WES_MosaicChildTumorP1Normal/
    rm -rf NGS.2_7_2b.WES_MosaicChildTumorP2Normal/
done < '/hpc/users/richtf01/gelb/mosaicism/data/sample_id.1.20.61.449.txt'
