#!/usr/bin/env bash
#Felix Richter
#7/8/2014
#cd /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
#delete existing anno screens
#screen -R -D anno1
#cd /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
#time bash /hpc/users/richtf01/gelb/mosaicism/src/snpeff.sh 2>&1 | tee snpeff.err.out.txt
#time bash /hpc/users/richtf01/gelb/mosaicism/src/snpeff_run.41.60.sh 2>&1 | tee snpeff.err.out.txt 

DATA_FILE='/hpc/users/richtf01/gelb/mosaicism/data/sample_id.41.60.txt'
SNPEFF_RUN_FILE='/hpc/users/richtf01/gelb/mosaicism/src/snpeff_run.41.60.sh'

rm $SNPEFF_RUN_FILE

while read SAMPLE_NAME; do
    echo "cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results" \
    >> $SNPEFF_RUN_FILE
    echo "java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \\" \
    >> $SNPEFF_RUN_FILE
    echo "-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \\" \
    >> $SNPEFF_RUN_FILE
    echo "GRCh37.74 $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.0102.singletons.vcf \\" \
    >> $SNPEFF_RUN_FILE
    echo "> $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.0102.singletons.anno.vcf" \
    >> $SNPEFF_RUN_FILE
done < $DATA_FILE

#mkdir /hpc/users/richtf01/www/00349/
#cd /hpc/users/richtf01/www/00349/
#ln -s /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results/snpEff_summary.html .
#user: 10m23.127s
