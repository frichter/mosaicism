#cd /sc/orga/projects/chdiTrios/PBG/PCGC/00307/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
#java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
#-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
#GRCh37.74 00307.00307.mutect.filtered.0102.singletons.vcf \
#> 00307.00307.mutect.filtered.0102.singletons.anno.vcf
#cd /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
#java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
#-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
#GRCh37.74 00349.00349.mutect.filtered.0102.singletons.vcf \
#> 00349.00349.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00352/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00352.00352.mutect.filtered.0102.singletons.vcf \
> 00352.00352.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00387/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00387.00387.mutect.filtered.0102.singletons.vcf \
> 00387.00387.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00424/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00424.00424.mutect.filtered.0102.singletons.vcf \
> 00424.00424.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00576/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00576.00576.mutect.filtered.0102.singletons.vcf \
> 00576.00576.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00655/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00655.00655.mutect.filtered.0102.singletons.vcf \
> 00655.00655.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00669/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00669.00669.mutect.filtered.0102.singletons.vcf \
> 00669.00669.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00711/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00711.00711.mutect.filtered.0102.singletons.vcf \
> 00711.00711.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00780/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00780.00780.mutect.filtered.0102.singletons.vcf \
> 00780.00780.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00796/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00796.00796.mutect.filtered.0102.singletons.vcf \
> 00796.00796.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00801/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00801.00801.mutect.filtered.0102.singletons.vcf \
> 00801.00801.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00819/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00819.00819.mutect.filtered.0102.singletons.vcf \
> 00819.00819.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00836/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00836.00836.mutect.filtered.0102.singletons.vcf \
> 00836.00836.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00852/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00852.00852.mutect.filtered.0102.singletons.vcf \
> 00852.00852.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00854/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00854.00854.mutect.filtered.0102.singletons.vcf \
> 00854.00854.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00875/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00875.00875.mutect.filtered.0102.singletons.vcf \
> 00875.00875.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00880/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00880.00880.mutect.filtered.0102.singletons.vcf \
> 00880.00880.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00907/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00907.00907.mutect.filtered.0102.singletons.vcf \
> 00907.00907.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00924/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00924.00924.mutect.filtered.0102.singletons.vcf \
> 00924.00924.mutect.filtered.0102.singletons.anno.vcf
