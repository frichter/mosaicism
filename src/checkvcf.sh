#!/bin/bash

while read SAMPLE_NAME;
do
	echo $SAMPLE_NAME
	cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results
	grep "##" $SAMPLE_NAME.combined.vcf | wc -l

done < '/sc/orga/work/landyk01/Gelblab/mosaicism/data/sample_id.uzilov.txt'
