#Felix Notes

#testing if change columns works
/sc/orga/projects/chdiTrios/PBG/PCGC/06168/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/06168.06168.mutect.vcf

cd /hpc/users/richtf01/gelb/mosaicism/src
#compare this
grep -A5 '#CHROM' /sc/orga/projects/chdiTrios/PBG/PCGC/06168/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/06168.06168.mutect.vcf
#to this:
bash change_columns.sh /sc/orga/projects/chdiTrios/PBG/PCGC/06168/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/06168.06168.mutect.vcf | head

#change columns works
#join the filtered mutect list with the anno file
awk 'NR==FNR{a[$1,$2]=$3;next} ($1,$2) in a{print $0, a[$1,$2]}' \
$SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.column.filtered $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.column \
> $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.filtered

#IF WE ARE USING MLL2 as a positive control:
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00392/Processed/NGS.2_7_0.WES
#create xmls
#create makefile