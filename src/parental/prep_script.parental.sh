#!/usr/bin/env bash
#running somatic variant calling pipeline
#looking for examples of de novo mosaicism causing a congenital defect
#ssh interactive2
#bash prep_script.parental.sh 2>&1 | tee prep_err_out.txt
#CODE_DIR='/sc/orga/work/landyk01/Gelblab/mosaicism/src/parental'
#CODE_DIR='/hpc/users/richtf01/gelb/mosaicism/src'
DATA_DIR='/sc/orga/projects/chdiTrios/PBG/PCGC'

while read SAMPLE_NAME; do
#SAMPLE_NAME=00349   
echo $SAMPLE_NAME

    #copy and edit xml and makefile
    #P1
    cd $DATA_DIR/$SAMPLE_NAME/Processed
    mkdir -p $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal
    cp $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/Makefile $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/
    cp $DATA_DIR/00349/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/00349.xml $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/$SAMPLE_NAME.xml
    #mv $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/00349.xml $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/$SAMPLE_NAME.xml
    sed -i "s/00349/$SAMPLE_NAME/g" $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/$SAMPLE_NAME.xml
    #python $CODE_DIR/xml_parse_P1.parental.py $SAMPLE_NAME $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/$SAMPLE_NAME.xml
    sed -i '/PEDIGREE/d' $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/Makefile
    sed -i 's/72:00:00/12:60:60/g' $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/Makefile
    sed -i '/TORQUE_ACCT/d' $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/Makefile
    sed -i 's/THREADS=16/THREADS=12/g' $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/Makefile
    sed -i '/THREADS/a QUEUE_MEM=8192m' $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/Makefile

    #P2
    cd $DATA_DIR/$SAMPLE_NAME/Processed
    mkdir -p $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP1TumorP2Normal
    cp $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/Makefile \
    $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP1TumorP2Normal
    cp $DATA_DIR/00349/Processed/NGS.2_7_2b.WES_MosaicP1TumorP2Normal/00349.xml $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP1TumorP2Normal/$SAMPLE_NAME.xml
    #mv $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/00349.xml $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/$SAMPLE_NAME.xml
    sed -i "s/00349/$SAMPLE_NAME/g" $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP1TumorP2Normal/$SAMPLE_NAME.xml
    #python $CODE_DIR/xml_parse_P2.parental.py $SAMPLE_NAME $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP1TumorP2Normal/$SAMPLE_NAME.xml
    sed -i '/PEDIGREE/d' $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP1TumorP2Normal/Makefile
    sed -i 's/72:00:00/12:60:60/g' $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP1TumorP2Normal/Makefile
    sed -i '/TORQUE_ACCT/d' $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP1TumorP2Normal/Makefile
    sed -i 's/THREADS=16/THREADS=12/g' $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP1TumorP2Normal/Makefile
    sed -i '/THREADS/a QUEUE_MEM=8192m' $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP1TumorP2Normal/Makefile

    #edit cohort lists
    #P1
    #mkdir -p $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/results
    #touch $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/results/$SAMPLE_NAME.cohort.list
    #replaced cohort.list file with:
#cat <<EOF > $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/$SAMPLE_NAME.cohort.list
#$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.$SAMPLE_NAME-01.clean.dedup.recal.bam
#$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.$SAMPLE_NAME-02.clean.dedup.recal.bam
#$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.$SAMPLE_NAME.clean.dedup.recal.bam
#EOF
    #P2
    #mkdir -p $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP1TumorP2Normal/results
    #touch $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP1TumorP2Normal/results/$SAMPLE_NAME.cohort.list
    #replaced cohort.list file with:
#cat <<EOF > $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/$SAMPLE_NAME.cohort.list
#$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.$SAMPLE_NAME-01.clean.dedup.recal.bam
#$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.$SAMPLE_NAME-02.clean.dedup.recal.bam
#$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.$SAMPLE_NAME.clean.dedup.recal.bam
#EOF
    #set permissions for everyone
    chmod -R a+rwx $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/
    chmod -R a+rwx $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP1TumorP2Normal/

done < '/sc/orga/work/landyk01/Gelblab/mosaicism/data/sample_id.parental.txt'
