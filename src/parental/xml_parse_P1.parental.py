#!/usr/bin/env python
#sys[1] is the smaple ID, sys[2] is the location of the xml file
#python xml_parse.py /sc/orga/work/richtf01/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal_practice/00030.xml
import xml.etree.cElementTree as ET
import sys
child_id = sys.argv[1]
P1_id = child_id + '-01'
P2_id = child_id + '-02'
xml_path = sys.argv[2]

with open(xml_path) as xml_file:
    tree = ET.parse(xml_file)
    root = tree.getroot()
    #fastq = root.find('fastq')
    #print fastq.get('id')
    fastq_list = root.findall('fastq')
    fastq_iter = iter(fastq_list)
    fastq1 = next(fastq_iter)
    fastq1.set('type','Tumor')
    fastq1.set('normal',P1_id)
    #check whether it should be 02 or 2
    fastq1.set('purity','0.05')
    for fastq in fastq_iter:
        if (fastq.get('id') == child_id):
            root.remove(fastq)
        else:
            fastq.set('type','Normal')
            break
    for fastq in fastq_iter:
        root.remove(fastq)
    #xml_out = xml_path[:-4] + "_test.xml"
    #tree.write(xml_out)
    tree.write(xml_path)
    #output loc: /sc/orga/work/richtf01/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal_practice/00030_test.xml
