#!/usr/bin/env bash
#filter the anno file
#Note that the mutect FORMAT tags my awk is for is GT:AD:BQ:DP:FA:SS
#bash filter.no-anno.parental.sh 2>&1 | tee filter.no-anno.err_out.txt

#REPO_DIR='/hpc/users/richtf01/gelb/mosaicism'
REPO_DIR='/sc/orga/work/landyk01/Gelblab/mosaicism'
DATA_FILE="${REPO_DIR}/data/sample_id.parental.txt"
#SAMPLE_NAME=00349

while read SAMPLE_NAME; do
    echo $SAMPLE_NAME
    #P1
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP2TumorP1Normal/results/
    #rm $SAMPLE_NAME.$SAMPLE_NAME-02.mutect.filtered.01.vcf
    #Check that we are searching for the correct format string (GT:AD:BQ:DP:FA:SS)
    #grep -v ^'#' $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf | cut -f 9 | sort | uniq -c
    #want kid first, then parent. switch here if necessary
    bash ${REPO_DIR}/src/parental/change_columns.parental.sh \
    $SAMPLE_NAME.$SAMPLE_NAME-02.mutect.vcf > $SAMPLE_NAME.$SAMPLE_NAME-02.mutect.vcf.column
    #save header from the COMBINED Annotation file
    awk '/^##/ {print $0}' $SAMPLE_NAME.$SAMPLE_NAME-02.mutect.vcf > $SAMPLE_NAME.$SAMPLE_NAME-02.mutect.vcf.header
    awk '/^#/ {print $0}' $SAMPLE_NAME.$SAMPLE_NAME-02.mutect.vcf.column >> $SAMPLE_NAME.$SAMPLE_NAME-02.mutect.vcf.header
    #filter THE MUTECT file (why? Because I designed my awk for the mutect, not anno file)
    grep PASS $SAMPLE_NAME.$SAMPLE_NAME-02.mutect.vcf.column | \
    awk -F '[\t:,]' 'BEGIN{OFS="\t"} {if ($24 >=3 && $24<=6 && $27 <= 0.20 && $26<80 && $26>=30  && $19<80 && $19 >= 25) print $0}' \
    > $SAMPLE_NAME.$SAMPLE_NAME-02.mutect.vcf.filtered
    #join header and filtered list
    cat $SAMPLE_NAME.$SAMPLE_NAME-02.mutect.vcf.filtered >> $SAMPLE_NAME.$SAMPLE_NAME-02.mutect.vcf.header
    mv $SAMPLE_NAME.$SAMPLE_NAME-02.mutect.vcf.header $SAMPLE_NAME.$SAMPLE_NAME-02.mutect.filtered.01.vcf
    rm $SAMPLE_NAME.$SAMPLE_NAME-02.mutect.vcf.{column,filtered}

    #P2
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicP1TumorP2Normal/results/
    #rm $SAMPLE_NAME.$SAMPLE_NAME-01.mutect.filtered.02.vcf
    #Check that we are searching for the correct format string (GT:AD:BQ:DP:FA:SS)
    #grep -v ^'#' $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf | cut -f 9 | sort | uniq -c
    #want kid first, then parent. switch here if necessary
    bash ${REPO_DIR}/src/parental/change_columns.parental.sh \
    $SAMPLE_NAME.$SAMPLE_NAME-01.mutect.vcf > $SAMPLE_NAME.$SAMPLE_NAME-01.mutect.vcf.column
    #save header from the COMBINED Annotation file
    awk '/^##/ {print $0}' $SAMPLE_NAME.$SAMPLE_NAME-01.mutect.vcf > $SAMPLE_NAME.$SAMPLE_NAME-01.mutect.vcf.header
    awk '/^#/ {print $0}' $SAMPLE_NAME.$SAMPLE_NAME-01.mutect.vcf.column >> $SAMPLE_NAME.$SAMPLE_NAME-01.mutect.vcf.header
    #filter THE MUTECT file (why? Because I designed my awk for the mutect, not anno file)
    grep PASS $SAMPLE_NAME.$SAMPLE_NAME-01.mutect.vcf.column | \
    awk -F '[\t:,]' 'BEGIN{OFS="\t"} {if ($17 >= 3 && $17<=6 && $20 <= 0.20 && $19<80 && $19 >= 30 && $26<80 && $26 >= 25) print $0}' \
    > $SAMPLE_NAME.$SAMPLE_NAME-01.mutect.vcf.filtered
    #join header and filtered list
    cat $SAMPLE_NAME.$SAMPLE_NAME-01.mutect.vcf.filtered >> $SAMPLE_NAME.$SAMPLE_NAME-01.mutect.vcf.header
    mv $SAMPLE_NAME.$SAMPLE_NAME-01.mutect.vcf.header $SAMPLE_NAME.$SAMPLE_NAME-01.mutect.filtered.02.vcf
    rm $SAMPLE_NAME.$SAMPLE_NAME-01.mutect.vcf.{column,filtered}

done < $DATA_FILE


