#!/usr/bin/env bash
#Felix Richter
#check the output of the make cancer pipeline
#bash pipeline.check.sh 2>&1 | tee pipeline.check.out_err.txt 

while read SAMPLE_NAME; do
#SAMPLE_NAME=00596
    echo $SAMPLE_NAME
    echo "P1"
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/
    grep -E "[0-9]*\sPend,\s[0-9]*\sRun,\s[0-9]*\sFail,\s[0-9]*" VariantCallingQ.makefile.log \
    | tail -n1
    grep -E "[0-9]*\sPend,\s[0-9]*\sRun,\s[0-9]*\sFail,\s[0-9]*\sDone" CancerQ.makefile.log \
    | tail -n1
    wc -l < $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf
    echo "P2"
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/
    grep -E "[0-9]*\sPend,\s[0-9]*\sRun,\s[0-9]*\sFail,\s[0-9]*" VariantCallingQ.makefile.log \
    | tail -n1
    grep -E "[0-9]*\sPend,\s[0-9]*\sRun,\s[0-9]*\sFail,\s[0-9]*\sDone" CancerQ.makefile.log \
    | tail -n1
    wc -l < $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf
done < '/sc/orga/work/landyk01/Gelblab/mosaicism/data/sample_id.parental.txt'




