#!/usr/bin/env bash
#running somatic variant calling pipeline
#looking for examples of de novo mosaicism causing a congenital defect
#ssh interactive2
#bash prep_script.sh 2>&1 | tee prep_err_out.txt
CODE_DIR='/hpc/users/richtf01/gelb/mosaicism/src'
DATA_DIR='/sc/orga/projects/chdiTrios/PBG/PCGC'
while read SAMPLE_NAME; do
    echo $SAMPLE_NAME
    chmod -R a+rwx $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/
    chmod -R a+rwx $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/
done < '/hpc/users/richtf01/gelb/mosaicism/data/sample_id.txt'

#test
#chmod -R a+rwx /sc/orga/projects/chdiTrios/PBG/PCGC/03407/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/
#chmod -R a+rwx /sc/orga/projects/chdiTrios/PBG/PCGC/03407/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/