#!/bin/bash
#script for blood first
module load vcftools

SAMPLE_NAME=m60
DIR_NAME=m60.m60_Heart
DATA_DIR='/sc/orga/projects/chdiTrios/PBG/heart_sweden'
OUT_DIR='/sc/orga/work/landyk01/Gelblab/mosaicism/data/paired_samples/results'


echo $SAMPLE_NAME
cd $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results
vcftools --vcf $SAMPLE_NAME.combined.vcf --extract-FORMAT-info GT 
grep -E '0/0\s0/1' out.GT.FORMAT > $OUT_DIR/$DIR_NAME/$SAMPLE_NAME.denovo_variants.txt
awk '/^#/ {print $0}' $SAMPLE_NAME.combined.vcf > $OUT_DIR/$DIR_NAME/$SAMPLE_NAME.denovo_variants.vcf
awk 'NR==FNR{a[$1,$2]=$3;next} ($1,$2) in a{print $0, a[$1,$2]}' $OUT_DIR/$DIR_NAME/$SAMPLE_NAME.denovo_variants.txt $SAMPLE_NAME.combined.vcf >> $OUT_DIR/$DIR_NAME/$SAMPLE_NAME.denovo_variants.vcf
cd $OUT_DIR/$DIR_NAME
vcftools --vcf $SAMPLE_NAME.denovo_variants.vcf --extract-FORMAT-info AD
cat out.AD.FORMAT | awk -F '[\t,]' 'BEGIN{OFS="\t"} {if ($6>=3) print $1, $2, $3, $4, $5, $6}' > $SAMPLE_NAME.denovo_variants.AD.txt
awk '/^#/ {print $0}' $SAMPLE_NAME.denovo_variants.vcf > $SAMPLE_NAME.denovo_variants.AD.vcf
awk 'NR==FNR{a[$1,$2]=$3;next} ($1,$2) in a{print $0, a[$1,$2]}' $SAMPLE_NAME.denovo_variants.AD.txt $SAMPLE_NAME.denovo_variants.vcf >> $SAMPLE_NAME.denovo_variants.AD.vcf
vcftools --vcf $SAMPLE_NAME.denovo_variants.AD.vcf --extract-FORMAT-info DP
cat out.AD.FORMAT | awk -F '[\t,]' 'BEGIN{OFS="\t"} {if ($3<80 && $3 >= 25) print $1, $2, $3, $4, $5, $6}' > $SAMPLE_NAME.denovo_variants.AD.DP.txt
awk '/^#/ {print $0}' $SAMPLE_NAME.denovo_variants.AD.vcf > $SAMPLE_NAME.denovo_variants.AD.DP.vcf
awk 'NR==FNR{a[$1,$2]=$3;next} ($1,$2) in a{print $0, a[$1,$2]}' $SAMPLE_NAME.denovo_variants.AD.DP.txt $SAMPLE_NAME.denovo_variants.AD.vcf >> $SAMPLE_NAME.denovo_variants.AD.DP.vcf
cat $SAMPLE_NAME.denovo_variants.AD.DP.txt | awk '{print $1, $2, $3, $4, $5, $6, $6/($5+$6) }' | awk '{if ($7 >= 0.15 && $7 <= 0.35) print $0}' | awk '{if ($4<2) print $0}' > $SAMPLE_NAME.mosaic_variants.filtered.txt
awk '/^#/ {print $0}' $SAMPLE_NAME.denovo_variants.AD.DP.vcf > $SAMPLE_NAME.mosaic_variants.filtered.vcf
awk 'NR==FNR{a[$1,$2]=$3;next} ($1,$2) in a{print $0, a[$1,$2]}' $SAMPLE_NAME.mosaic_variants.filtered.txt  $SAMPLE_NAME.denovo_variants.AD.DP.vcf >> $SAMPLE_NAME.mosaic_variants.filtered.vcf
