#!/usr/bin/env bash
#filter the anno file
#Note that the mutect FORMAT tags my awk is for is GT:AD:BQ:DP:FA:SS
#bash filter.no-anno.paired.sh 2>&1 | tee -a filter.no-anno.err_out.txt

SAMPLE_NAME=m30.m30_Heart
#while read SAMPLE_NAME; do
    echo $SAMPLE_NAME
    #P1
     cd /sc/orga/work/landyk01/Gelblab/mosaicism/data/paired_samples/results/$SAMPLE_NAME
    #cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/
    #Check that we are searching for the correct format string (GT:AD:BQ:DP:FA:SS)
    #grep -v ^'#' $SAMPLE_NAME.$SAMPLE_NAME_Heart.mutect.vcf | cut -f 9 | sort | uniq -c
    #want kid first, then parent. switch here if necessary
    #bash /hpc/users/richtf01/gelb/mosaicism/src/change_columns.sh \
    #$SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf > $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.column
    #filter THE MUTECT file (why? Because I designed my awk for the mutect, not anno file)
    #grep PASS $SAMPLE_NAME.mutect.vcf | \
    #awk -F '[\t:,]' 'BEGIN{OFS="\t"} {if ($24 >= 3 && $27 >= 0.15 && $27 <= 0.35 && $19<80 && $19 >= 25) print $1, $2}' \
    #> $SAMPLE_NAME.mutect.vcf.filtered
    grep PASS $SAMPLE_NAME.mutect.vcf | \
    awk -F '[\t:,]' 'BEGIN{OFS="\t"} {if ($17 >= 3 && $20 >= 0.15 && $20 <= 0.35 && $26<80 && $26 >= 25) print $1, $2}' \
    > $SAMPLE_NAME.mutect.vcf.filtered
    #JOIN SELECTED COLUMNS WITH FULL MUTECT
    #save header from the COMBINED Annotation file
    awk '/^#/ {print $0}' $SAMPLE_NAME.mutect.vcf > $SAMPLE_NAME.mutect.vcf.header
    #join the filtered mutect list with the anno file
    awk 'NR==FNR{a[$1,$2]=$3;next} ($1,$2) in a{print $0, a[$1,$2]}' \
    $SAMPLE_NAME.mutect.vcf.filtered $SAMPLE_NAME.mutect.vcf \
    > $SAMPLE_NAME.mutect.vcf.filtered.2
    #join header and filtered list
    cat $SAMPLE_NAME.mutect.vcf.filtered.2 >> $SAMPLE_NAME.mutect.vcf.header
    mv $SAMPLE_NAME.mutect.vcf.header $SAMPLE_NAME.mutect.filtered.vcf
    rm $SAMPLE_NAME.mutect.vcf.{filtered,filtered.2}

    #P2
    #cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/
    #Check that we are searching for the correct format string (GT:AD:BQ:DP:FA:SS)
    #grep -v ^'#' $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf | cut -f 9 | sort | uniq -c
    #want kid first, then parent. switch here if necessary
    #bash /hpc/users/richtf01/gelb/mosaicism/src/change_columns.sh \
    #$SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf > $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.column
    #filter THE MUTECT file (why? Because I designed my awk for the mutect, not anno file)
    #grep PASS $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.column | \
    #awk -F '[\t:,]' 'BEGIN{OFS="\t"} {if ($17 >= 3 && $20 >= 0.15 && $20 <= 0.35 && $26<80 && $26 >= 25) print $1, $2}' \
    #> $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.column.filtered
    #JOIN SELECTED COLUMNS WITH FULL MUTECT
    #save header from the COMBINED Annotation file
    #awk '/^#/ {print $0}' $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf > $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.header
    #join the filtered mutect list with the anno file
    #awk 'NR==FNR{a[$1,$2]=$3;next} ($1,$2) in a{print $0, a[$1,$2]}' \
    #$SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.column.filtered $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf \
    #> $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.filtered
    #join header and filtered list
    #cat $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.filtered >> $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.header
    #mv $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.header $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.02.vcf
    #rm $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.{column,column.filtered,filtered}

#done < '/sc/orga/work/landyk01/Gelblab/mosaicism/data/paired_samples/paired_sample_names.txt'
