#!/usr/bin/env bash
#$1 is file

SAMPLE= '/sc/orga/work/landyk01/Gelblab/mosaicism/data/paired_samples/results/m34.m34_Heart/m34.m34_Heart.mutect.vcf'
COL_10=$(grep '#CHROM' $SAMPLE | cut -f 10)
COL_11=$(grep '#CHROM' $SAMPLE | cut -f 11)

if [[ $COL_10 =~ d$ ]] ; then
    grep -v ^'##' $SAMPLE | \
    awk -F '\t' 'BEGIN{OFS="\t"} { print $1,$2,$3,$4,$5,$6,$7,$8,$9,$11,$10}' > \
    /sc/orga/work/landyk01/Gelblab/mosaicism/data/paired_samples/results/m34.m34_Heart/m34.m34_Heart.mutect.column.vcf
fi

if [[ $COL_11 =~ d$ ]] ; then 
    grep -v ^'##' $SAMPLE | \
    awk -F '\t' 'BEGIN{OFS="\t"} { print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11}' > \
    /sc/orga/work/landyk01/Gelblab/mosaicism/data/paired_samples/results/m34.m34_Heart/m34.m34_Heart.mutect.column.vcf
fi
