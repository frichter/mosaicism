#!/usr/bin/env bash
#Felix Richter
#ssh interactive2
#cd /sc/orga/projects/chdiTrios/PBG/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/
#screen -ls
#kill other screens
#screen -R -D pipeline
#cd /sc/orga/projects/chdiTrios/PBG/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/
#module load ngs/2.7.2b
#bash /hpc/users/richtf01/gelb/mosaicism/src/screen_commands.1.20.sh

while read SAMPLE_NAME; do
    echo $SAMPLE_NAME
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/
    make results/$SAMPLE_NAME.vcf.list &
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/
    make results/$SAMPLE_NAME.vcf.list &
done < '/hpc/users/richtf01/gelb/mosaicism/data/sample_id.1.20.txt'
