#!/usr/bin/env bash

module load ngs/2.7.2b
module load vcftools/0.1.12a

rm -rf /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results/
#Copy files to new directory
mkdir -p /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results/
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results/
cp /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/00349.combined.anno.vcf .
mv 00349.combined.anno.vcf 00349.combined.anno.01.vcf
cp /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/00349.combined.anno.vcf .
mv 00349.combined.anno.vcf 00349.combined.anno.02.vcf
echo "copied"



#PASS 
vcftools --vcf 00349.combined.anno.01.vcf --remove-filtered-all --minDP 25 \
--maxDP 80 --recode --out 00349.combined.anno.01.pass.dp
mv 00349.combined.anno.01.pass.dp.recode.vcf 00349.combined.anno.01.pass.dp.vcf 
vcftools --vcf 00349.combined.anno.02.vcf --remove-filtered-all --minDP 25 \
--maxDP 80 --recode --out 00349.combined.anno.02.pass.dp
mv 00349.combined.anno.02.pass.dp.recode.vcf 00349.combined.anno.02.pass.dp.vcf 

#GT Filter
vcftools --vcf 00349.combined.anno.01.pass.dp.vcf --extract-FORMAT-info GT --out 01
grep -E '0\/1\s0\/0\s0\/0' 01.GT.FORMAT > 01.singletons
awk '/^#/ {print $0}' 00349.combined.anno.01.pass.dp.vcf > 00349.combined.anno.01.pass.dp.vcf.header
awk 'NR==FNR{a[$1,$2]=$3;next} ($1,$2) in a{print $0, a[$1,$2]}' \
01.singletons 00349.combined.anno.PASS.01.vcf \
>> 00349.combined.anno.01.pass.dp.vcf.header
mv 00349.combined.anno.01.pass.dp.vcf.header 00349.combined.anno.01.pass.dp.singletons.vcf

vcftools --vcf 00349.combined.anno.02.pass.dp.vcf --extract-FORMAT-info GT --out 02
grep -E '0\/1\s0\/0\s0\/0' 02.GT.FORMAT > 02.singletons
awk '/^#/ {print $0}' 00349.combined.anno.02.pass.dp.vcf > 00349.combined.anno.02.pass.dp.vcf.header
awk 'NR==FNR{a[$1,$2]=$3;next} ($1,$2) in a{print $0, a[$1,$2]}' \
02.singletons 00349.combined.anno.PASS.02.vcf \
>> 00349.combined.anno.02.pass.dp.vcf.header
mv 00349.combined.anno.02.pass.dp.vcf.header 00349.combined.anno.02.pass.dp.singletons.vcf

#AD
vcftools --vcf 00349.combined.anno.01.pass.dp.vcf --extract-FORMAT-info AD --out 01
awk '{if ($3 >= 3) print $0}' 01.AD.FORMAT > 01.AD
awk '/^#/ {print $0}' 00349.combined.anno.01.pass.dp.vcf > 00349.combined.anno.01.pass.dp.vcf.header
awk 'NR==FNR{a[$1,$2]=$3;next} ($1,$2) in a{print $0, a[$1,$2]}' \
01.singletons 00349.combined.anno.PASS.01.vcf \
>> 00349.combined.anno.01.pass.dp.vcf.header
mv 00349.combined.anno.01.pass.dp.vcf.header 00349.combined.anno.01.pass.dp.singletons.vcf

vcftools --vcf 00349.combined.anno.02.pass.dp.vcf --extract-FORMAT-info AD --out 02
grep -E '0\/1\s0\/0\s0\/0' 02.AD.FORMAT > 02.AD
awk '/^#/ {print $0}' 00349.combined.anno.02.pass.dp.vcf > 00349.combined.anno.02.pass.dp.vcf.header
awk 'NR==FNR{a[$1,$2]=$3;next} ($1,$2) in a{print $0, a[$1,$2]}' \
02.singletons 00349.combined.anno.PASS.02.vcf \
>> 00349.combined.anno.02.pass.dp.vcf.header
mv 00349.combined.anno.02.pass.dp.vcf.header 00349.combined.anno.02.pass.dp.singletons.vcf

#AF


#Take intersection of GT, AD

