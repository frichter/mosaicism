#!/usr/bin/env bash
#Felix Richter
#6/23/2014
#processing of bash files after running pipeline

#ssh interactive1
#make sure there is no other anno screen running, kill it if it's done
#screen -R -D anno
#bash vcf-merge.sh 2>&1 | tee -a vcf-merge.no-anno.err_out.txt

SAMPLE_NAME='00349'

SAMPLE_DIR='/sc/orga/projects/chdiTrios/PBG/PCGC'
DATA_FILE='/hpc/users/richtf01/gelb/mosaicism/data/sample_id.21.60.txt'
OUT_DIR='/hpc/users/richtf01/gelb/mosaicism/results'
rm -rf $OUT_DIR
mkdir $OUT_DIR

module load ngs/2.7.2b
module load vcftools/0.1.12a

echo $SAMPLE_NAME
#merge files
mkdir -p $SAMPLE_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results/
cd $SAMPLE_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results/
cp $SAMPLE_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/$SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.01.vcf .
cp $SAMPLE_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/$SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.02.vcf .
bgzip $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.01.vcf
tabix -p vcf $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.01.vcf.gz
bgzip $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.02.vcf
tabix -p vcf $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.02.vcf.gz
vcf-merge $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.01.vcf.gz $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.02.vcf.gz \
| bgzip -c > $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.0102.vcf.gz

#decompress everything
bgzip -d $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.01.vcf.gz
bgzip -d $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.02.vcf.gz
bgzip -d $SAMPLE_NAME.$SAMPLE_NAME.mutect.filtered.0102.vcf.gz

echo $SAMPLE_NAME | tee -a $OUT_FILE
grep -v '#' 00349.00349.mutect.filtered.0102.vcf | \
grep -v -E '\.\s\.' | tee -a /hpc/users/richtf01/gelb/mosaicism/results/00349.txt
