#!/bin/bash

mkdir -p run_jobs
rm -rf run_jobs/run_job_*.P2.lsf

mkdir -p run_jobs
while read SAMPLE_NAME; do
    echo $SAMPLE_NAME
    #P2
    echo '#!/bin/bash' > run_jobs/run_job_$SAMPLE_NAME.P2.lsf

    echo "#BSUB -J \"$SAMPLE_NAME.P2\"" >> run_jobs/run_job_$SAMPLE_NAME.P2.lsf
    echo '#BSUB -q scavenger
#BSUB -n 1
#BSUB -o %J.out
#BSUB -u felix.richter@mssm.edu
#BSUB -W 06

module load ngs/2.7.2b

' >> run_jobs/run_job_$SAMPLE_NAME.P2.lsf

    echo "cd /sc/orga/projects/chdiTrios/PBG/PCGC/"\
"$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/"\
     >> run_jobs/run_job_$SAMPLE_NAME.P2.lsf
    echo "make results/$SAMPLE_NAME.vcf.list"\
     >> run_jobs/run_job_$SAMPLE_NAME.P2.lsf

done < '/hpc/users/richtf01/gelb/mosaicism/data/sample_id.1.20.txt'
