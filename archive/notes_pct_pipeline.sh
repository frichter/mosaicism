#!/usr/bin/env bash
#running somatic variant calling pipeline
#looking for examples of de novo mosaicism causing a congenital defect
DATA_DIR = /sc/orga/work/richtf01/
#grep for sample names
ls /sc/orga/projects/chdiTrios/PBG/PCGC | grep -E '\<[0-9]{5}\>'$ > /hpc/users/richtf01/gelb/mosaicism/data/sample_id.txt

#workflow number 1
ssh interactive2
cd $DATA_DIR/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal
screen -R -D cancer_test
module load ngs/2.7.2b
make -n results/00030.vcf.list | grep ^queue
make results/00030.vcf.list
#detach from screen ctrl-A, d
cat results/VariantCallingQ.makefile.log
cat results/CancerQ.makefile.log

#workflow number two
ssh interactive2
cd $DATA_DIR/richtf01/PCGC/00030/Processed
mkdir NGS.2_7_2b.WES_MosaicChildTumorP2Normal
cp $DATA_DIR/PCGC/00030/Processed/NGS.2_7_0.WES/{00030.xml,Makefile} \
$DATA_DIR/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal
#edit the Makefile
sed -i '/PEDIGREE/d' $DATA_DIR/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/Makefile
sed -i 's/72:00:00/12:60:60/g' $DATA_DIR/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/Makefile
#python script remove all but first instance of every ID
#removed ID for 1 but not 2
#added type='Tumor' normal='00030-2' purity='0.05' to the first instance of ID
#added type='Normal' to first instance of second ID
mkdir $DATA_DIR/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results
cp $DATA_DIR/PCGC/00030/Processed/NGS.2_7_0.WES/results/00030.cohort.list \
$DATA_DIR/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results
#replaced cohort.list file with:
cat <<EOF > $DATA_DIR/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/00030.cohort.list
$DATA_DIR/PCGC/00030/Processed/NGS.2_7_0.WES/results/00030.00030-01.clean.dedup.recal.bam
$DATA_DIR/PCGC/00030/Processed/NGS.2_7_0.WES/results/00030.00030-02.clean.dedup.recal.bam
$DATA_DIR/PCGC/00030/Processed/NGS.2_7_0.WES/results/00030.00030.clean.dedup.recal.bam
EOF

cd $DATA_DIR/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal
screen -R -D pipeline
#screen -d -m -S pipeline
#screen -S pipeline -X module load ngs/2.7.2b
#screen -S pipeline -X make results/00030.vcf.list
module load ngs/2.7.2b
make -n results/00030.vcf.list | grep ^queue
make results/00030.vcf.list
#detach from screen ctrl-A, d
cat results/VariantCallingQ.makefile.log
cat results/CancerQ.makefile.log

cd $DATA_DIR/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/
screen -R -D anno
module load ngs/2.7.2b
make annotations

cat 00030.combined.anno.vcf | grep PASS > 00030.combined.anno.pass.vcf
module load vcftools/0.1.12a
cd $DATA_DIR/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/
bgzip 00030.combined.anno.pass.vcf
tabix -p vcf 00030.combined.anno.pass.vcf.gz
cd $DATA_DIR/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/
bgzip 00030.combined.anno.vcf
tabix -p vcf 00030.combined.anno.vcf.gz
mkdir -p $DATA_DIR/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results/
cd $DATA_DIR/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results/
vcf-merge \
$DATA_DIR/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/00030.combined.anno.vcf.gz \
$DATA_DIR/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/B.vcf.gz \
| bgzip -c > 00030.combined.anno.merged
#real    51m58.566s
#user    1m8.035s
#sys     0m16.202s

bash run_ALL_jobs.sh 2>&1 | tee -a run_err_out.txt

head -n 40 sample_id.txt | tail -n 20

#files with failed runs
grep -lr 'QCommandLine - Script failed with 121 total jobs' .
./1511035.out
./1511034.out
./1511044.out
./1511046.out
./1511047.out
./1511045.out
./1511049.out
./1511048.out
./1511056.out
./1511057.out
grep -rl '99 Pend, 20 Run, 0 Fail, 2 Done' . | sort

#files with failed runs 
grep -A1 '1511034\|1511035\|1511044\|1511045\|1511046\|1511047\|1511048\|1511049\|1511056\|1511057' run_err_out.txt
Job <1511034> is submitted to queue <scavenger>.
started job ./run_job_00307.P1.lsf
Job <1511035> is submitted to queue <scavenger>.
started job ./run_job_00307.P2.lsf
--
Job <1511044> is submitted to queue <scavenger>.
started job ./run_job_00576.P1.lsf
Job <1511045> is submitted to queue <scavenger>.
started job ./run_job_00576.P2.lsf
Job <1511046> is submitted to queue <scavenger>.
started job ./run_job_00655.P1.lsf
Job <1511047> is submitted to queue <scavenger>.
started job ./run_job_00655.P2.lsf
Job <1511048> is submitted to queue <scavenger>.
started job ./run_job_00669.P1.lsf
Job <1511049> is submitted to queue <scavenger>.
started job ./run_job_00669.P2.lsf
--
Job <1511056> is submitted to queue <scavenger>.
started job ./run_job_00801.P1.lsf
Job <1511057> is submitted to queue <scavenger>.
started job ./run_job_00801.P2.lsf

BSUB -e /sc/orga/scratch/richtf01/

#update tree with
git add --all :/
#prepping data
#head -n20 sample_id.txt | tail -n17 > sample_id.4.20.txt

#start pipeline in screen. Saved in screen_commands.sh
while read SAMPLE_NAME; do
    echo $SAMPLE_NAME
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/
    make results/$SAMPLE_NAME.vcf.list
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/
    make results/$SAMPLE_NAME.vcf.list
done < '/hpc/users/richtf01/gelb/mosaicism/data/sample_id.1.1.txt'

#6/24 jobs  bjobs 1515965 1515966

ssh interactive2
screen -R -D anno
module load ngs/2.7.2b
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal
make annotations &
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal
make annotations &


sed -n 81,150p sample_id.txt > sample_id.81.150.txt


#create a directory for Linderman
mkdir /sc/orga/scratch/richtf01/prep_files_temp
cd prep_files_temp
mkdir NGS.2_7_2b.WES_MosaicChildTumorP1Normal
cd NGS.2_7_2b.WES_MosaicChildTumorP1Normal
cp /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/Makefile .
cp /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/00349.xml .
mkdir results
cp /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/00349.cohort.list ./results/

cd /sc/orga/scratch/richtf01/prep_files_temp
mkdir NGS.2_7_2b.WES_MosaicChildTumorP2Normal
cd NGS.2_7_2b.WES_MosaicChildTumorP2Normal
cp /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/Makefile .
cp /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/00349.xml .
mkdir results
cp /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/00349.cohort.list ./results/

setfacl -m u:pateln35:rw- /sc/orga/scratch/richtf01/prep_files_temp

#sanity checks
grep REJECT 00349.00349.combined.anno.vcf.final
#none
tail -n1 00349.00349.combined.anno.vcf.final
#looks good









