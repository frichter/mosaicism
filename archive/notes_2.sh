#P1
cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/
#START WITH MUTECT
#confirm all lines are the same- they probably aren't
grep -v ^'#' $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf | cut -f 9 | sort | uniq -c
#want kid first, then parent. switch here if necessary
bash /hpc/users/richtf01/gelb/mosaicism/src/change_columns.sh \
$SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf > $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.column
#filter THE MUTECT file (why? Because I designed my awk for the mutect, not anno file)
grep PASS $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.column | \
awk -F '[\t:,]' 'BEGIN{OFS="\t"} {if ($16 >= 3 && $20 <= 0.3 && $26<80 && $26 >= 25) print $1, $2}' \
> $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.column.filtered
#JOIN WITH ANNO
#save header from the COMBINED Annotation file
awk '/^#/ {print $0}' $SAMPLE_NAME.combined.anno.vcf > $SAMPLE_NAME.combined.anno.vcf.header
#join the filtered mutect list with the anno file
awk 'NR==FNR{a[$1,$2]=$3;next} ($1,$2) in a{print $0, a[$1,$2]}' \
$SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.column.filtered $SAMPLE_NAME.combined.anno.vcf \
> $SAMPLE_NAME.combined.anno.vcf.filtered
#join header and filtered list
cat $SAMPLE_NAME.combined.anno.vcf.filtered >> $SAMPLE_NAME.combined.anno.vcf.header
mv $SAMPLE_NAME.combined.anno.vcf.header $SAMPLE_NAME.$SAMPLE_NAME.combined.anno.vcf.final
#sanity checks.
wc -l $SAMPLE_NAME.combined.anno.vcf.filtered
wc -l $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.column.filtered
tail $SAMPLE_NAME.combined.anno.vcf.filtered
tail $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.column.filtered
#WHY AM I MISSING SOME VALUES in the anno file? These two should be equal

cd /sc/orga/projects/chdiTrios/PBG/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal
QUEUE_MEM=8192m
module load ngs/2.7.2b

cd /sc/orga/projects/chdiTrios/PBG/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal
make results/00030.vcf.list &