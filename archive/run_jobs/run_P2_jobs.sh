#!/bin/bash
#bash run_P2_jobs.sh 2>&1 | tee -a run_P2_err_out.txt

for f in ./run_job_*.P2.lsf; do
    bsub < ./$f
    #use < to interpret bsub cookies in script
    echo "started job $f"
done
bjobs -u richtf01 | echo
