cd /sc/orga/projects/chdiTrios/PBG/PCGC/00940/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00940.00940.mutect.filtered.0102.singletons.vcf \
> 00940.00940.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00949/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00949.00949.mutect.filtered.0102.singletons.vcf \
> 00949.00949.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00958/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 00958.00958.mutect.filtered.0102.singletons.vcf \
> 00958.00958.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/01004/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 01004.01004.mutect.filtered.0102.singletons.vcf \
> 01004.01004.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/01014/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 01014.01014.mutect.filtered.0102.singletons.vcf \
> 01014.01014.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/01056/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 01056.01056.mutect.filtered.0102.singletons.vcf \
> 01056.01056.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/01100/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 01100.01100.mutect.filtered.0102.singletons.vcf \
> 01100.01100.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/01110/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 01110.01110.mutect.filtered.0102.singletons.vcf \
> 01110.01110.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/01120/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 01120.01120.mutect.filtered.0102.singletons.vcf \
> 01120.01120.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/01123/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 01123.01123.mutect.filtered.0102.singletons.vcf \
> 01123.01123.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/01130/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 01130.01130.mutect.filtered.0102.singletons.vcf \
> 01130.01130.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/01158/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 01158.01158.mutect.filtered.0102.singletons.vcf \
> 01158.01158.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/01164/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 01164.01164.mutect.filtered.0102.singletons.vcf \
> 01164.01164.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/01196/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 01196.01196.mutect.filtered.0102.singletons.vcf \
> 01196.01196.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/01197/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 01197.01197.mutect.filtered.0102.singletons.vcf \
> 01197.01197.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/01221/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 01221.01221.mutect.filtered.0102.singletons.vcf \
> 01221.01221.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/01231/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 01231.01231.mutect.filtered.0102.singletons.vcf \
> 01231.01231.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/01242/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 01242.01242.mutect.filtered.0102.singletons.vcf \
> 01242.01242.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/01255/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 01255.01255.mutect.filtered.0102.singletons.vcf \
> 01255.01255.mutect.filtered.0102.singletons.anno.vcf
cd /sc/orga/projects/chdiTrios/PBG/PCGC/01291/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results
java -Xmx8G -jar /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.jar \
-c /sc/orga/projects/PBG/Resources/AnnoL.test_dbNSFP_update/snpEff/snpEff.config \
GRCh37.74 01291.01291.mutect.filtered.0102.singletons.vcf \
> 01291.01291.mutect.filtered.0102.singletons.anno.vcf
