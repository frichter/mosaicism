#!/usr/bin/env bash
#filter the anno file
#Note that the mutect FORMAT tags my awk is for is GT:AD:BQ:DP:FA:SS

SAMPLE_NAME='00349'

echo '00349'
#P1
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/
#Check that we are searching for the correct format string
grep -v ^'#' 00349.00349.mutect.vcf | cut -f 9 | sort | uniq -c
#want kid first, then parent. switch here if necessary
bash /hpc/users/richtf01/gelb/mosaicism/src/change_columns.sh \
00349.00349.mutect.vcf > 00349.00349.mutect.vcf.column
#filter THE MUTECT file (why? Because I designed my awk for the mutect, not anno file)
grep PASS 00349.00349.mutect.vcf.column | \
awk -F '[\t:,]' 'BEGIN{OFS="\t"} {print $1, $2}' \
> 00349.00349.mutect.vcf.column.filtered
#JOIN SELECTED COLUMNS WITH FULL MUTECT
#save header from the COMBINED Annotation file
awk '/^#/ {print $0}' 00349.00349.mutect.vcf > 00349.00349.mutect.vcf.header
#join the filtered mutect list with the anno file
awk 'NR==FNR{a[$1,$2]=$3;next} ($1,$2) in a{print $0, a[$1,$2]}' \
00349.00349.mutect.vcf.column.filtered 00349.00349.mutect.vcf \
> 00349.00349.mutect.vcf.filtered
#join header and filtered list
cat 00349.00349.mutect.vcf.filtered >> 00349.00349.mutect.vcf.header
mv 00349.00349.mutect.vcf.header 00349.00349.mutect.filtered.02.vcf
rm 00349.00349.mutect.vcf.{column,column.filtered,filtered}

#if ($16 >= 3 && $20 >= 0.15 && $20 <= 0.35 && $26<80 && $26 >= 25) 
#if ($16 >= 3 && $20 >= 0.15 && $20 <= 0.35 && $26<80 && $26 >= 25) 
