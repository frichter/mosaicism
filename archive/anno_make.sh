#!/usr/bin/env bash
#Felix Richter
#ssh interactive2
#cd /sc/orga/projects/chdiTrios/PBG/PCGC/00046/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/
#screen -R -D anno
#cd /sc/orga/projects/chdiTrios/PBG/PCGC/00046/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/
#module load ngs/2.7.2b
#bash /hpc/users/richtf01/gelb/mosaicism/src/make_anno.sh 

while read SAMPLE_NAME; do
    echo $SAMPLE_NAME
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal
    make annotations &
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal
    make annotations &
done < '/hpc/users/richtf01/gelb/mosaicism/data/sample_id.1.20.61.449.txt'
