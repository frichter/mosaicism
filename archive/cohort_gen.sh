#!/usr/bin/env bash
#running somatic variant calling pipeline
#looking for examples of de novo mosaicism causing a congenital defect
#ssh interactive2
#bash make_cohort.sh 2>&1 | tee -a cohort_err_out.txt
CODE_DIR='/hpc/users/richtf01/gelb/mosaicism/src'
DATA_DIR='/sc/orga/projects/chdiTrios/PBG/PCGC'
while read SAMPLE_NAME; do
    echo $SAMPLE_NAME

    #edit cohort lists
    #P1
    mkdir -p $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results
    #cp $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.cohort.list \
    #$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results
    touch $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/$SAMPLE_NAME.cohort.list
    #replaced cohort.list file with:
cat <<EOF > $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/$SAMPLE_NAME.cohort.list
$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.$SAMPLE_NAME-01.clean.dedup.recal.bam
$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.$SAMPLE_NAME-02.clean.dedup.recal.bam
$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.$SAMPLE_NAME.clean.dedup.recal.bam
EOF
    #P2
    mkdir -p $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results
    #cp $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.cohort.list \
    #$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results
    touch $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/$SAMPLE_NAME.cohort.list
    #replaced cohort.list file with:
cat <<EOF > $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/$SAMPLE_NAME.cohort.list
$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.$SAMPLE_NAME-01.clean.dedup.recal.bam
$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.$SAMPLE_NAME-02.clean.dedup.recal.bam
$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.$SAMPLE_NAME.clean.dedup.recal.bam
EOF

done < '/hpc/users/richtf01/gelb/mosaicism/data/sample_id.txt'
