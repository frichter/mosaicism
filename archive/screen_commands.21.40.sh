#!/usr/bin/env bash
#Felix Richter
#ssh interactive1
#cd /sc/orga/projects/chdiTrios/PBG/PCGC/00307/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/
#screen -ls
#screen -R -D pipeline
#cd /sc/orga/projects/chdiTrios/PBG/PCGC/00307/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/
#module load ngs/2.7.2b
#bash /hpc/users/richtf01/gelb/mosaicism/src/screen_commands.21.40.sh 

while read SAMPLE_NAME; do
    echo $SAMPLE_NAME
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/
    make results/$SAMPLE_NAME.vcf.list &
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/
    make results/$SAMPLE_NAME.vcf.list &
done < '/hpc/users/richtf01/gelb/mosaicism/data/sample_id.21.40.txt'

#test
#must cd in before opening up screen session, otherwise permission denied
#screen -R -D pipeline
#cd /sc/orga/projects/chdiTrios/PBG/PCGC/00046/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/
#make results/00046.vcf.list &
#cd /sc/orga/projects/chdiTrios/PBG/PCGC/00046/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/
#make results/00046.vcf.list &

#cat /sc/orga/projects/chdiTrios/PBG/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/VariantCallingQ.makefile.log 
#cat /sc/orga/projects/chdiTrios/PBG/PCGC/00030/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/VariantCallingQ.makefile.log 


#tail -n10 /sc/orga/projects/chdiTrios/PBG/PCGC/00064/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/VariantCallingQ.makefile.log 
#cat /sc/orga/projects/chdiTrios/PBG/PCGC/00064/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/VariantCallingQ.makefile.log 

