
#http://vcftools.sourceforge.net/
#http://vcftools.sourceforge.net/perl_module.html
module load ngs/2.7.2b
module load vcftools/0.1.12a

#make a merged vcf file
mkdir -p /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results/
cd /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1P2_Merged/results/
cp /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/00349.00349.combined.anno.01.vcf .
cp /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/00349.00349.combined.anno.02.vcf .
bgzip 00349.00349.combined.anno.01.vcf
tabix -p vcf 00349.00349.combined.anno.01.vcf.gz
bgzip 00349.00349.combined.anno.02.vcf
tabix -p vcf 00349.00349.combined.anno.02.vcf.gz
vcf-merge 00349.00349.combined.anno.01.vcf.gz 00349.00349.combined.anno.02.vcf.gz \
| bgzip -c > 00349.00349.combined.anno.merged.vcf.gz

#decompress everything
bgzip -d 00349.00349.combined.anno.01.vcf.gz
bgzip -d 00349.00349.combined.anno.02.vcf.gz
bgzip -d 00349.00349.combined.anno.merged.vcf.gz

if column 10-12 are periods, check 
cut -f9-15 00349.00349.combined.anno.merged.vcf
grep -v '#' 00349.00349.combined.anno.merged.vcf | grep -v -E '\.\s\.\s\.'

vcf-isec -o -n 2 00349.00349.combined.anno.01.vcf.gz 00349.00349.combined.anno.02.vcf.gz | bgzip -c > 00349.00349.combined.anno.intersect.vcf.gz 
bgzip -d 00349.00349.combined.anno.intersect.vcf.gz



vcftools --vcf 00349.combined.anno.vcf --remove-filtered-all --out test_PASS --recode 



grep -v ^'#' 00349.00349.mutect.vcf | cut -f 9 | sort | uniq -c
#want kid first, then parent. switch here if necessary
bash /hpc/users/richtf01/gelb/mosaicism/src/change_columns.sh \
00349.00349.mutect.vcf > 00349.00349.mutect.vcf.column
#filter THE MUTECT file (why? Because I designed my awk for the mutect, not anno file)
grep PASS 00349.00349.mutect.vcf.column | \
awk -F '[\t:,]' 'BEGIN{OFS="\t"} {if ($16 >= 3 && $20 >= 0.15 && $20 <= 0.35 && $26<80 && $26 >= 25) print $1, $2}' \
> 00349.00349.mutect.vcf.column.filtered
#JOIN WITH ANNO
#save header from the COMBINED Annotation file
awk '/^#/ {print $0}' 00349.combined.anno.vcf > 00349.combined.anno.vcf.header
#join the filtered mutect list with the anno file
awk 'NR==FNR{a[$1,$2]=$3;next} ($1,$2) in a{print $0, a[$1,$2]}' \
00349.00349.mutect.vcf.column.filtered 00349.combined.anno.vcf \
> 00349.combined.anno.vcf.filtered
#join header and filtered list
cat 00349.combined.anno.vcf.filtered >> 00349.combined.anno.vcf.header
mv 00349.combined.anno.vcf.header 00349.00349.combined.anno.01.vcf

awk '/^#/ {print $0}' 00349.combined.anno.vcf > 00349.combined.anno.vcf.header
grep PASS 00349.00349.combined.anno.vcf >> 00349.combined.anno.vcf.header
mv 00349.combined.anno.vcf.header 00349.00349.combined.anno.PASS.01.vcf
tail 00349.combined.anno.PASS.01.vcf

cd /sc/orga/projects/chdiTrios/PBG/PCGC/00349/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results
awk '/^#/ {print $0}' 00349.combined.anno.vcf > 00349.combined.anno.vcf.header
grep PASS 00349.00349.combined.anno.vcf >> 00349.combined.anno.vcf.header
mv 00349.combined.anno.vcf.header 00349.00349.combined.anno.PASS.02.vcf

vcftools --vcf 00349.00349.combined.anno.PASS.01.vcf --extract-FORMAT-info GT --out 01.GT
grep -E '0\/1\s0\/0\s0\/0' 01.GT.GT.FORMAT
awk '/^#/ {print $0}' 00349.00349.combined.anno.PASS.01.vcf > 00349.00349.combined.anno.PASS.01.vcf.header
awk 'NR==FNR{a[$1,$2]=$3;next} ($1,$2) in a{print $0, a[$1,$2]}' \
test_singleton.singletons 00349.00349.combined.anno.PASS.01.vcf \
>> 00349.00349.combined.anno.PASS.01.vcf.header

#add the header
vcftools --vcf 00349.00349.combined.anno.PASS.01.vcf.singletons --extract-FORMAT-info AD --out 01_out
awm

vcftools --vcf  00349.00349.combined.anno.PASS.01.vcf.singletons --minDP 25 --maxDP 80 --recode
vcftools --vcf 00349.00349.combined.anno.PASS.02.vcf --singletons --out 02_singleton

0/0:82,0:82:PASS:99:0,448,9074

#filter by DP (read depth)
vcftools --vcf  00349.00349.combined.anno.PASS.01.vcf.singletons --minDP 25 --maxDP 80 --recode-INFO-all
#filter singletons
vcftools --vcf 00349.00349.combined.anno.PASS.01.vcf --extract-FORMAT-info GT --out 01
grep -E '0\/1\s0\/0\s0\/0' 01.GT.FORMAT > 01.singletons
#create header
awk '/^#/ {print $0}' 00349.00349.combined.anno.PASS.01.vcf > 00349.00349.combined.anno.PASS.01.vcf.header
#join singletons and append to header
awk 'NR==FNR{a[$1,$2]=$3;next} ($1,$2) in a{print $0, a[$1,$2]}' \
01.singletons 00349.00349.combined.anno.PASS.01.vcf \
>> 00349.00349.combined.anno.PASS.01.vcf.header
mv 00349.00349.combined.anno.PASS.01.vcf.header 00349.00349.combined.anno.PASS.01.singletons.vcf
#awk hack 
awk '' 00349.00349.combined.anno.PASS.01.singletons.vcf


#Merge
bgzip 00349.combined.anno.01.vcf
tabix -p vcf 00349.combined.anno.01.vcf.gz
bgzip 00349.combined.anno.02.vcf
tabix -p vcf 00349.combined.anno.02.vcf.gz
vcf-merge 00349.combined.anno.01.vcf.gz 00349.combined.anno.02.vcf.gz \
| bgzip -c > 00349.combined.anno.merged.vcf.gz
echo "merged"
bgzip -d 00349.combined.anno.01.vcf.gz
bgzip -d 00349.combined.anno.02.vcf.gz
bgzip -d 00349.combined.anno.merged.vcf.gz



rm 00349.combined.anno.01.vcf.header
awk '/^#/ {print $0}' 00349.combined.anno.01.vcf > 00349.combined.anno.01.vcf.header
grep PASS 00349.combined.anno.01.vcf >> 00349.combined.anno.01.vcf.header
mv 00349.combined.anno.01.vcf.header 00349.combined.anno.01.vcf

rm 00349.combined.anno.02.vcf.header
awk '/^#/ {print $0}' 00349.combined.anno.02.vcf > 00349.combined.anno.02.vcf.header
grep PASS 00349.combined.anno.02.vcf >> 00349.combined.anno.02.vcf.header
mv 00349.combined.anno.02.vcf.header 00349.combined.anno.02.vcf

grep -v -E '^#' 00349.combined.anno.merged.vcf | wc -l 
grep -v -E '^#' 00349.combined.anno.01.vcf | wc -l 
grep -v -E '^#' 00349.combined.anno.02.vcf | wc -l 

#rm 00349.combined.anno.merged.vcf.header
#awk '/^#/ {print $0}' 00349.combined.anno.merged.vcf > 00349.combined.anno.merged.vcf.header
#grep PASS 00349.combined.anno.merged.vcf >> 00349.combined.anno.merged.vcf.header
#mv 00349.combined.anno.merged.vcf.header 00349.combined.anno.merged.vcf


