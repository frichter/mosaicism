#!/usr/bin/env bash
#temp csv output
OUT_FILE=/hpc/users/richtf01/gelb/mosaicism/data/filter.1.20.41.80.csv

#echo "Trio, P1 Variants Filtered, P1 Total Variants, P2 Variants Filtered, P2 Total Variants" > $OUT_FILE
while read SAMPLE_NAME; do
    echo $SAMPLE_NAME
    #P1
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/
    P1_VAR_FILTERED=$(wc -l < $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.column.filtered)
    P1_VAR_TOTAL=$(grep -v ^'#' $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf | grep PASS | wc -l)
    #P2
    cd /sc/orga/projects/chdiTrios/PBG/PCGC/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/
    P2_VAR_FILTERED=$(wc -l < $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf.column.filtered)
    P2_VAR_TOTAL=$(grep -v ^'#' $SAMPLE_NAME.$SAMPLE_NAME.mutect.vcf | grep PASS | wc -l)
    echo "$SAMPLE_NAME, $P1_VAR_FILTERED, $P1_VAR_TOTAL, $P2_VAR_FILTERED, $P2_VAR_TOTAL" >> $OUT_FILE
done < '/hpc/users/richtf01/gelb/mosaicism/data/sample_id.41.80.txt'
