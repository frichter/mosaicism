#!/usr/bin/env bash
#running somatic variant calling pipeline
#looking for examples of de novo mosaicism causing a congenital defect
#ssh interactive2
#bash prep_script.cohort_mod.sh 2>&1 | tee -a prep.cohort_mod.err_out.txt
CODE_DIR='/hpc/users/richtf01/gelb/mosaicism/src'
DATA_DIR='/sc/orga/projects/chdiTrios/PBG/PCGC'
while read SAMPLE_NAME; do
    echo $SAMPLE_NAME

    #copy and edit xml and makefile
    #P1
    cd $DATA_DIR/$SAMPLE_NAME/Processed
    mkdir -p $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal
    cp $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/{$SAMPLE_NAME.xml,Makefile} $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/
    python $CODE_DIR/xml_parse_P1.py $SAMPLE_NAME $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/$SAMPLE_NAME.xml
    sed -i '/PEDIGREE/d' $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/Makefile
    sed -i 's/72:00:00/12:60:60/g' $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/Makefile

    #P2
    cd $DATA_DIR/$SAMPLE_NAME/Processed
    mkdir -p $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal
    cp $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/{$SAMPLE_NAME.xml,Makefile} \
    $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal
    python $CODE_DIR/xml_parse_P2.py $SAMPLE_NAME $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/$SAMPLE_NAME.xml
    sed -i '/PEDIGREE/d' $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/Makefile
    sed -i 's/72:00:00/12:60:60/g' $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/Makefile

    #edit cohort lists
    #P1
    mkdir -p $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results
    touch $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/$SAMPLE_NAME.cohort.list
    #replaced cohort.list file with:
cat <<EOF > $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP1Normal/results/$SAMPLE_NAME.cohort.list
$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.$SAMPLE_NAME-01.clean.dedup.recal.bam
$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.$SAMPLE_NAME.clean.dedup.recal.bam
EOF
    #P2
    mkdir -p $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results
    touch $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/$SAMPLE_NAME.cohort.list
    #replaced cohort.list file with:
cat <<EOF > $DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_2b.WES_MosaicChildTumorP2Normal/results/$SAMPLE_NAME.cohort.list
$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.$SAMPLE_NAME-02.clean.dedup.recal.bam
$DATA_DIR/$SAMPLE_NAME/Processed/NGS.2_7_0.WES/results/$SAMPLE_NAME.$SAMPLE_NAME.clean.dedup.recal.bam
EOF

done < '/hpc/users/richtf01/gelb/mosaicism/data/sample_id.41.60.txt'



